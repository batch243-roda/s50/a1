import { Fragment, useState } from 'react';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Courses from './pages/Courses';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import Logout from './pages/Logout';
import Page404 from './pages/Page404';
import { useUserContext } from './hooks/useUserContext';

function App() {
  const { user } = useUserContext();

  return (
    <Fragment>
      <BrowserRouter>
        <AppNavbar />
        <Routes>
          <Route path="/" element={<Home />} />

          <Route path="/courses" element={<Courses />} />

          <Route
            path="/login"
            element={!user ? <Login /> : <Navigate to="/" />}
          />

          <Route
            path="/register"
            element={!user ? <Register /> : <Navigate to="/" />}
          />
          <Route path="/logout" element={<Logout />} />

          <Route path="*" element={<Page404 />} />
        </Routes>
      </BrowserRouter>
    </Fragment>
  );
}

export default App;
