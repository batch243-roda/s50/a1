import React, { useEffect, useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { useUserContext } from '../hooks/useUserContext';

const Register = () => {
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [isEmpty, setIsEmpty] = useState(true);

  const { setUser } = useUserContext();

  useEffect(() => {
    if (email && password1 && password2 && password1 === password2) {
      setIsEmpty(false);
    } else {
      setIsEmpty(true);
    }
  }, [email, password1, password2]);

  const handleSubmit = (e) => {
    e.preventDefault();
    alert(`Congratulations, ${email} successfully registered!`);

    localStorage.setItem('email', email);

    setUser(localStorage.getItem('email'));

    setEmail('');
    setPassword1('');
    setPassword2('');
  };

  return (
    <Container>
      <Row className="justify-content-center p-5">
        <Col sm={12} md={6} className="bg-dark p-3 rounded text-light">
          <Form onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="email">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                onChange={(e) => setEmail(e.target.value)}
                value={email}
                required
              />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                onChange={(e) => setPassword1(e.target.value)}
                value={password1}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="confirmPassword">
              <Form.Label>Confirm Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                onChange={(e) => setPassword2(e.target.value)}
                value={password2}
                required
              />
            </Form.Group>
            <Button variant="primary" type="submit" disabled={isEmpty}>
              Submit
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

export default Register;
