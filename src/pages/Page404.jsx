import React from 'react';
import { Link } from 'react-router-dom';

const Page404 = () => {
  return (
    <div className="d-flex flex-column justify-content-center align-items-center p-5 m-5">
      <h1 className=" display-1">Page404</h1>
      <p>Page Not Found</p>
      <Link to="/">Go Home</Link>
    </div>
  );
};

export default Page404;
