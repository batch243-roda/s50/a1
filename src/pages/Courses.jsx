import React from 'react';
import { Row } from 'react-bootstrap';
import CourseCard from '../components/CourseCard';
import coursesData from '../data/course';

const Courses = () => {
  // console.log(coursesData);
  return coursesData.map((course) => (
    <CourseCard key={course.id} coursesData={course} />
  ));
};

export default Courses;
