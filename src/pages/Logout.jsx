import { useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import { useUserContext } from '../hooks/useUserContext';

const Logout = () => {
  const { removeUser } = useUserContext();
  useEffect(() => {
    removeUser();
  }, []);

  return <Navigate to="/" />;
};

export default Logout;
